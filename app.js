const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

const linksRouter = require('./routers/shortlinks');

require('./db/mongoose');
app.use(express.json());
app.use(linksRouter);

app.listen(port);