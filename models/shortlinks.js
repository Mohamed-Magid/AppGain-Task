const mongoose = require('mongoose');

const shortLinksSchema = new mongoose.Schema({
    slug: {
        type: String,
        required: true,
        unique: true,
        validate: /^[a-zA-Z0-9]+$/ //alphanumeric
    },
    ios: {
        primary: {
            type: String
        },
        fallback: {
            type: String
        }
    },
    android: {
        primary: {
            type: String
        },
        fallback: {
            type: String
        }
    },
    web: {
        type: String
    }
});


// Delete mongoose's _id and __v from response
shortLinksSchema.methods.toJSON = function () {
    const linksObject = this.toObject();
    delete linksObject._id;
    delete linksObject.__v;
    return linksObject;
};

const ShortLinks = mongoose.model('ShortLinks', shortLinksSchema);

module.exports = ShortLinks;