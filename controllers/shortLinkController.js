const _ = require('lodash'); // Used in update shortlinks
const ShortLink = require('../models/shortlinks');
const { slugHandler } = require('../helpers/slugHandler');

const createSlug = async (req, res) => {
    req.body.slug = await slugHandler(req.body);
    const slug = new ShortLink(req.body);
    try {
        // If the content type header is not JSON return 400 with JSON header
        if (req.headers['content-type'] !== 'application/json') {
            res.status(400)
                .json({
                    "status": "failed",
                    "message": "Bad Request"
                });
        } else {
            // If the content type header is  json return 201 with JSON header
            await slug.save();
            res.status(201).send({
                status: "successful",
                slug: req.body.slug,
                message: "created successfully"
            });
        }
    } catch (e) {
        // If an error occur return 500
        res.status(500).header('content-type', 'application/json').send({});
    }
};

const readSlugs = async (req, res) => {
    try {
        const slugs = await ShortLink.find({}); // Get all shortlinks

        if (req.headers['content-type'] !== 'application/json') {
            res.status(400)
                .json({
                    "status": "failed",
                    "message": "Bad Request"
                });
        } else if (slugs.length === 0) {
            res.status(404)
                .json({
                    "status": "failed",
                    "message": "not found"
                });
        } else {
            res.status(200).send({
                shortlinks: slugs
            });
        }
    }
    catch (e) {
        res.status(500).json({});
    }
};

const updateSlug = async (req, res) => {
    if (req.headers['content-type'] !== 'application/json') {
        res.status(400)
            .json({
                "status": "failed",
                "message": "Bad Request"
            });
    }
    try {
        const link = await ShortLink.findOne({ slug: req.params.slug });
        if (!link) {
            res.status(404)
                .json({
                    "status": "failed",
                    "message": "not found"
                });
        }
        else {
            const updates = Object.keys(req.body); // Turn all req.body keys into array
            const allowedUpdates = ['ios', 'android', 'web']; // Allowed updates for the user to make (Excluding Slugs)
            const isValidUpdate = updates.every((update) => allowedUpdates.includes(update)); // Check if all updates are valid

            if (!isValidUpdate) {
                // If user edited slug return 400 with proper error message
                res.status(404)
                .json({
                    "status": "failed",
                    "message": "cannot edit slug"
                });
            }
            else {
                // If all updates are valid use lodash merge to merge the current document with user updates
                _.merge(link, req.body);
                await link.save();
                res.status(201)
                .json({
                    "status": "successful",
                    "message": "updated successfully"
                });
            }
        }
    }
    catch (e) {
        res.status(500).json({});
    }
};

const deleteSlug = async (req, res) => {
    if (req.headers['content-type'] !== 'application/json') {
        res.status(400)
            .json({
                "status": "failed",
                "message": "Bad Request"
            });
    }
    try {
        const link = await ShortLink.findOne({ slug: req.params.slug });
        if (!link) {
            res.status(404)
                .json({
                    "status": "failed",
                    "message": "not found"
                });
        } else {
            await link.remove();
            res.status(200)
                .json({
                    "status": "deleted",
                    "message": "deleted successfully"
                });
        }
    } catch (e) {
        res.status(500).json({});        
    }
};
module.exports = {
    createSlug,
    readSlugs,
    updateSlug,
    deleteSlug
};