const random = require('randomstring'); // npm module to generate random strings
const ShortLinks = require('../models/shortlinks');


const slugHandler = async (body) => {
    if (body.slug) {
        if (await isDuplicate(body.slug)) {
            return random.generate(6);            
        }
        else {
            return body.slug;
        }
    }
    else {
        return random.generate(6);
    }
    
};

// Check whether there's a duplicate slug in the db or not
const isDuplicate = async (slug) => {
    try {
        const duplicate = await ShortLinks.findOne({ slug });
        if (duplicate)
            return true;
    
        else
            return false;
    }
    catch (e) {
        return e;
    }
    
};

module.exports = { slugHandler };