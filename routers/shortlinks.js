const express = require('express');
const router = new express.Router();
const shortLinkController = require('../controllers/shortLinkController');

// Create Shortlinks
router.post('/shortlinks', shortLinkController.createSlug);

// List Shortlinks
router.get('/shortlinks', shortLinkController.readSlugs);

// Update Shortlinks
router.put('/shortlinks/:slug', shortLinkController.updateSlug);

// Delete Shortlinks
router.delete('/shortlinks/:slug', shortLinkController.deleteSlug);

// Redirect to appgain website
router.all('/', (req, res) => {
    res.redirect('https://www.appgain.io/');
});

module.exports = router;