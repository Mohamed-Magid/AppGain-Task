# AppGain Task
This app was created as a technical task for [AppGain](https://www.appgain.io/) full-stack internship role.

## API
It's a simple link shortner API to create shortlinks associated with actual links for Android, iOS and Web.

### Features
1. **Create shortlinks**:
* To create a new shortlink make an http ``POST request`` using **Postman** to this end-point: `https://appgain-task.herokuapp.com/shortlinks`.
* Make sure that you've set a `Content-Type: application/json` header or you'll get a 400 http status code.
* Provide a JSON body on this form:
```JSON
{
  "slug": "s5G1f3",
  "ios": {
    "primary": "http://...",
    "fallback": "http://..."
  },
  "android": {
    "primary": "http://...",
    "fallback": "http://..."
  },
  "web": "http://..."
}
```
* If the slug user provided is already stored in the database or not provided by the user the API will automatically generates a random slug.

2. **Read all shortlinks:**
* To read all shortlinks make an http ``GET request`` using **Postman** to this end-point: `https://appgain-task.herokuapp.com/shortlinks`.
* Make sure that you've set a `Content-Type: application/json` header or you'll get a 400 http status code.

3. **Update shortlinks:**
* To update a shortlink make an http ``PUT request`` using **Postman** to this end-point: `https://appgain-task.herokuapp.com/shortlinks/{slug}` where user replaces {slug} with the actual slug he wants to update its links.
* Provide a JSON body with only with the attributes desired to be updated.
For example, if user wants to update iOS fallback attribute:
```JSON
{
  "ios": {
    "fallback": "http://..."
  }
}
```
* Make sure that you've set a `Content-Type: application/json` or you'll get a 400 http status code.
* Please note that: Slug attribute cannot be updated.

4. **Delete shortlinks:**
* To delete a shortlink make an http ``DELETE request`` using **Postman** to this end-point: ``https://appgain-task.herokuapp.com/shortlinks/{slug}`` here user replaces {slug} with the actual slug he wants to delete its links.
* Make sure that you've set a `Content-Type: application/json` or you'll get a 400 http status code.

**Please note that if user notices any delay on the first request that's because of heroku's free dynos forces the app to sleep if it doesn't receive any traffic in 1 hour. More info from [here](https://blog.heroku.com/app_sleeping_on_heroku).**